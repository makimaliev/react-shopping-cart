export const formatPrice = (x, currency) => {
  switch (currency) {
    case 'BRL':
      return x.toFixed(2).replace('.', ',');
    default:
      return x.toFixed(2);
  }
};

//export const productsAPI = 'http://localhost:8000/api/v1/products/';
//export const productsAPI = "http://localhost:8001/api/products";
export const productsAPI = "http://localhost:8080/api/products/list"

